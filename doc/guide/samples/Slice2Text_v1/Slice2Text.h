//==============================================================================
/*
 * This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 *
 * Medical Data Segmentation Toolkit (MDSTk)    \n
 * Copyright (c) 2003-2010 by Michal Spanel     \n
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2006/10/30                          \n
 *
 * File description:
 * - Sample module.
 */

#ifndef MDS_SLICE2TEXT_H
#define MDS_SLICE2TEXT_H

// MDSTk
#include <MDSTk/Module/mdsModule.h>


//==============================================================================
/*!
 * Sample module providing conversion of an input image to text (ASCII art).
 */
class CSlice2Text : public mds::mod::CModule
{
public:
    //! Smart pointer type.
    //! - Declares type tSmartPtr.
    MDS_SHAREDPTR(CSlice2Text);

public:
    //! Default constructor.
    CSlice2Text(const std::string& sDescription);

    //! Virtual destructor.
    virtual ~CSlice2Text();

protected:
    //! Virtual method called on startup.
    virtual bool startup();

    //! Virtual method called by the processing thread.
    virtual bool main();

    //! Called on console shutdown.
    virtual void shutdown();

    //! Called on writing an usage statement.
    virtual void writeExtendedUsage(std::ostream& Stream);

protected:
    //! Color inversion.
    bool m_bInvert;

    //! Characters.
    std::string m_ssGray2Char;
};


//==============================================================================
/*!
 * Smart pointer to console application.
 */
typedef CSlice2Text::tSmartPtr    CSlice2TextPtr;


#endif // MDS_SLICE2TEXT_H

