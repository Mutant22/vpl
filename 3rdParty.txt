Used 3rd Party Libraries
------------------------

Here is a list of all external 3rd party libraries used by VPL:

- Eigen library for linear algebra: vectors, matrices, and related algorithms
  by Benoit Jacob and Gael Guennebaud, et al.
  http://eigen.tuxfamily.org/
  
- FFTW (Fast Fourier Transform in the West) library by Matteo Frigo
  and Steven G. Johnson 
  http://www.fftw.org/
  
- UMFPACK library by Timothy A. Davis
  http://www.cise.ufl.edu/research/sparse/umfpack/  
  
- JPEG library by Independent JPEG Group
  http://www.ijg.org/
  
- libpng library by Glenn Randers-Pehrson et al.
  http://www.libpng.org/pub/png/
  
- zlib library by JEan-loup Gailly and Mark Adler
  http://www.zlib.net/
  
- Freglut library - The Free OpenGL Utility Toolkit
  http://freeglut.sourceforge.net/ 

Some of these libraries are distributed under a more restrictive license
then the VPL one. For example the FFTW library uses the GNU GPL license.
If you feel restricted with any of these licenses, you can disable particular
libraries during the CMake configuration step.

Please see licenses of individual 3rd party libraries for more details.
