@echo off 
rem Please, modify and run the 'wsetenv.bat' batch file first in order to
rem setup path to built binaries, etc.

echo Sobel filter...
mdsLoadJPEG <berounka.jpg |mdsSliceFilter -filter sobely |mdsSliceRange -auto |mdsSliceView

echo Sobel filter magnitude...
mdsLoadJPEG <berounka.jpg |mdsSliceFilter -filter sobely -norm abs |mdsSliceRange -auto |mdsSliceView

echo Median filter...
mdsLoadJPEG <berounka.jpg |mdsSliceFilter -filter median -size 5 |mdsSliceRange -auto |mdsSliceView

echo Canny edge detector...
mdsLoadJPEG <berounka.jpg |mdsSliceEdgeDetector -detector canny -t1 0.3 -t2 0.1 |mdsSliceRange -auto |mdsSliceView

