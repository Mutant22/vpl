#!/bin/sh
# Please, modify and run either the 'lsetenv.sh' or the 'msetenv.sh' shell
# script first in order to setup path to built binaries, etc.

echo "Image FFT..."
mdsLoadDicom <../data/dicom/80.dcm |mdsSliceFFT -result abs -shift -logspectrum |mdsSliceRange -auto |mdsSliceView
