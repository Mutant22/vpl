#!/bin/sh
# Please, modify and run either the 'lsetenv.sh' or the 'msetenv.sh' shell
# script first in order to setup path to built binaries, etc.

echo "Load JPEG image as a medical slice..."
mdsLoadJPEG <berounka.jpg |mdsSliceRange -auto |mdsSliceView

echo "Load JPEG image as a RGB image..."
mdsLoadJPEG -format rgb <berounka.jpg |mdsRGBImageView

echo "Histogram equalization..."
mdsLoadJPEG <berounka.jpg |mdsSliceHistEqualization |mdsSliceRange -auto |mdsSliceView

echo "Affine transform..."
mdsLoadJPEG <berounka.jpg |mdsSliceAffineTransform -alpha 50.0 -sx 2 -sy 2 -fill -interpolator bicubic |mdsSliceRange -auto |mdsSliceView
