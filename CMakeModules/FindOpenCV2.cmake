# - Try to find OpenCV library installation
# See http://sourceforge.net/projects/opencvlibrary/
#
# The following variables are optionally searched for defaults
#  OpenCV_ROOT_DIR : Base directory of OpenCV tree to use.
#  OpenCV_FIND_REQUIRED_COMPONENTS : FIND_PACKAGE(OpenCV COMPONENTS ..) 
#    compatible interface. Typically opencv_core opencv_imgproc etc.
#
# The following variables are set after configuration is done: 
#  OpenCV_FOUND
#  OpenCV_INCLUDE_DIR
#  OpenCV_LIBRARIES
#  OpenCV_LINK_DIRECTORIES
#
# 2004/05 Jan Woetzel, Friso, Daniel Grest 
# 2006/01 complete rewrite by Jan Woetzel
# 2006/09 2nd rewrite introducing ROOT_DIR and PATH_SUFFIXES 
#   to handle multiple installed versions gracefully by Jan Woetzel
# 2009/05 modified by Michal Spanel for OpenCV 2.0
# 2010/11 modified by Michal Spanel for OpenCV 2.1
# 2011/01 modified by Michal Spanel for OpenCV 2.2
# 2012/02 modified by Michal Spanel for OpenCV 2.3.1
#
# tested with:
# - OpenCV 2.2:  MSVC 9.0
# - OpenCV 2.3.1:  MSVC 10.0
#
# www.mip.informatik.uni-kiel.de/~jw
# --------------------------------


# required OpenCV components
IF( NOT OpenCV_FIND_REQUIRED_COMPONENTS )
  # default
  SET( OpenCV_FIND_REQUIRED_COMPONENTS opencv_core opencv_imgproc opencv_features2d opencv_gpu opencv_calib3d opencv_objdetect opencv_video opencv_highgui opencv_ml opencv_legacy opencv_contrib opencv_flann )
ENDIF( NOT OpenCV_FIND_REQUIRED_COMPONENTS )


# Try to find OpenCV on Windows
get_filename_component( OpenCV_POSSIBLE_ROOT_DIR
  "[HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\OpenCV2.2;UninstallString]"
  PATH
  )

# typical root dirs of installations, exactly one of them is used
if( UNIX )
  SET (OpenCV_POSSIBLE_ROOT_DIRS
    "${OpenCV_ROOT_DIR}"
    "$ENV{OpenCV_ROOT_DIR}"
    "${OpenCV_POSSIBLE_ROOT_DIR}"
    /usr/local
    /usr
    )
else( UNIX )
  SET (OpenCV_POSSIBLE_ROOT_DIRS
    "${OpenCV_ROOT_DIR}"
    "$ENV{OpenCV_ROOT_DIR}"
    "${OpenCV_POSSIBLE_ROOT_DIR}"
    "$ENV{ProgramFiles}/OpenCV"
    "$ENV{ProgramFiles}/OpenCV2"
    "C:/OpenCV"
    "D:/OpenCV"
    "C:/OpenCV2"
    "D:/OpenCV2"
    )
endif( UNIX )


# header include dir suffixes appended to OpenCV_ROOT_DIR
SET(OpenCV_INCDIR_SUFFIXES
  include
  include/opencv
  include/opencv-2.3.1
  3rdparty/include
  )

# library linkdir suffixes appended to OpenCV_ROOT_DIR 
SET(OpenCV_LIBDIR_SUFFIXES
  lib
  3rdparty/lib
  build/lib
  )


#
# select exactly ONE OpenCV base directory/tree 
# to avoid mixing different version headers and libs
#
FIND_PATH(OpenCV_ROOT_DIR 
  NAMES include/opencv2/opencv.hpp
  PATHS ${OpenCV_POSSIBLE_ROOT_DIRS}
  PATH_SUFFIXES ${OpenCV_INCDIR_SUFFIXES} )


# some dditional directories...
if( WIN32 )
  if( MSVC )
    # MS Visual Studio
    if( CMAKE_CL_64 )
      if( MSVC_VERSION EQUAL "1500" )
        # VC9
        set( OpenCV_LIBDIR_SUFFIXES build/x64/vc9/lib ${OpenCV_LIBDIR_SUFFIXES} )
      elseif( MSVC_VERSION EQUAL "1600" )
        # VC10
        set( OpenCV_LIBDIR_SUFFIXES build/x64/vc10/lib ${OpenCV_LIBDIR_SUFFIXES} )
      endif()
    else( CMAKE_CL_64 )
      if( MSVC_VERSION EQUAL "1500" )
        # VC9
        set( OpenCV_LIBDIR_SUFFIXES build/x86/vc9/lib ${OpenCV_LIBDIR_SUFFIXES} )
      elseif( MSVC_VERSION EQUAL "1600" )
        # VC10
        set( OpenCV_LIBDIR_SUFFIXES build/x86/vc10/lib ${OpenCV_LIBDIR_SUFFIXES} )
      endif()
    endif( CMAKE_CL_64 )
  else( MSVC )
    # MinGW compiler
    set( OpenCV_LIBDIR_SUFFIXES build/x86/mingw/lib ${OpenCV_LIBDIR_SUFFIXES} )
  endif( MSVC )
endif( WIN32 )

#
# find incdir
#
FIND_PATH( OpenCV_BASE_INCLUDE_DIR
  NAMES opencv2/opencv.hpp
  PATHS ${OpenCV_ROOT_DIR}
  PATH_SUFFIXES ${OpenCV_INCDIR_SUFFIXES} )

#
# find sbsolute path to all libraries 
# some are optionally, some may not exist on Linux
#
foreach( NAME ${OpenCV_FIND_REQUIRED_COMPONENTS} )
  FIND_LIBRARY( OpenCV_${NAME}_LIBRARY
    NAMES ${NAME} ${NAME}220 ${NAME}231
    PATHS ${OpenCV_ROOT_DIR}
    PATH_SUFFIXES ${OpenCV_LIBDIR_SUFFIXES} )
  FIND_LIBRARY( OpenCV_${NAME}d_LIBRARY
    NAMES ${NAME} ${NAME}220d ${NAME}231d
    PATHS ${OpenCV_ROOT_DIR}
    PATH_SUFFIXES ${OpenCV_LIBDIR_SUFFIXES} )
endforeach( NAME )

#
# Logic selecting required libs and headers
#
SET( OpenCV_FOUND ON )
SET( OpenCV_INCLUDE_DIR
  ${OpenCV_BASE_INCLUDE_DIR}
  ${OpenCV_BASE_INCLUDE_DIR}/opencv 
  ${OpenCV_BASE_INCLUDE_DIR}/../build/include 
  ${OpenCV_BASE_INCLUDE_DIR}/../3rdparty/include )
SET( OpenCV_LIBRARIES "" )
FOREACH( NAME ${OpenCV_FIND_REQUIRED_COMPONENTS} )
  # only good if all libraries found   
  IF( OpenCV_${NAME}d_LIBRARY AND OpenCV_${NAME}_LIBRARY )
    LIST( APPEND OpenCV_LIBRARIES debug ${OpenCV_${NAME}d_LIBRARY} optimized ${OpenCV_${NAME}_LIBRARY} )
    MARK_AS_ADVANCED( OpenCV_${NAME}d_LIBRARY )
    MARK_AS_ADVANCED( OpenCV_${NAME}_LIBRARY )
  ELSE( OpenCV_${NAME}d_LIBRARY AND OpenCV_${NAME}_LIBRARY )
    IF( OpenCV_${NAME}_LIBRARY )
      LIST( APPEND OpenCV_LIBRARIES ${OpenCV_${NAME}_LIBRARY} )    
      MARK_AS_ADVANCED( OpenCV_${NAME}_LIBRARY )
    ELSE( OpenCV_${NAME}_LIBRARY )
      SET( OpenCV_FOUND OFF )
    ENDIF( OpenCV_${NAME}_LIBRARY )
  ENDIF( OpenCV_${NAME}d_LIBRARY AND OpenCV_${NAME}_LIBRARY )
ENDFOREACH( NAME )

# get the link directory for rpath to be used with LINK_DIRECTORIES: 
IF (OpenCV_opencv_core_LIBRARY)
  GET_FILENAME_COMPONENT(OpenCV_LINK_DIRECTORIES ${OpenCV_opencv_core_LIBRARY} PATH)
ENDIF (OpenCV_opencv_core_LIBRARY)

MARK_AS_ADVANCED(
  OpenCV_ROOT_DIR
  OpenCV_BASE_INCLUDE_DIR
  )


# display help message
IF (NOT OpenCV_FOUND)
  # make FIND_PACKAGE friendly
  IF (NOT OpenCV_FIND_QUIETLY)
    IF(OpenCV_FIND_REQUIRED)
      MESSAGE(FATAL_ERROR "OpenCV required but some headers or libs not found. Please specify it's location with OpenCV_ROOT_DIR env. variable.")
    ELSE(OpenCV_FIND_REQUIRED)
      MESSAGE(STATUS "ERROR: OpenCV was not found.")
    ENDIF(OpenCV_FIND_REQUIRED)
  ENDIF(NOT OpenCV_FIND_QUIETLY)
ENDIF (NOT OpenCV_FOUND)

