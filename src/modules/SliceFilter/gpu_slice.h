#ifndef VPL_IF_SOBEL_H
#include <VPL/Image/ImageFiltering.h>
#endif

//OpenCL
#include <CL/cl.hpp>

#include <time.h>

//TIMER
#include <chrono>
 
#define TIMING
 
#ifdef TIMING
#define INIT_TIMER auto start = std::chrono::high_resolution_clock::now();
#define START_TIMER  start = std::chrono::high_resolution_clock::now();
#define STOP_TIMER(name)  std::cerr << "RUNTIME of " << name << ": " << \
    std::chrono::duration_cast<std::chrono::nanoseconds>( \
            std::chrono::high_resolution_clock::now()-start \
    ).count() << " ns " << std::endl; 
#else
#define INIT_TIMER
#define START_TIMER
#define STOP_TIMER(name)
#endif
//END TIMER

typedef struct
{
    bool is_Valid;
    cl::Device selected_Device;
} tDevice;


namespace vpl
{
namespace img
{

template <class I, template <typename> class N = CDefaultFilterPolicy>
class MyCSobelX : public CSobelX<I,N> //CNormImageFilter<I,N>
{
public:
    typedef CSobelX<I,N> base; //CNormImageFilter<I,N> base;
    typedef typename base::tImage tImage;
    typedef typename base::tPixel tPixel;
    typedef typename base::tResult tResult;
public:
    //constructor
    MyCSobelX() {}

    //! Destructor.
    ~MyCSobelX() {}
    
    bool operator()(const tImage& SrcImage, tImage& DstImage, tDevice& gpu_dev);
};

template <typename I, template <typename> class N>
bool MyCSobelX<I,N>::operator()(const tImage& SrcImage, tImage& DstImage, tDevice& gpu_dev)
{
    
    INIT_TIMER;
    // Image size
    tSize XCount = vpl::math::getMin(SrcImage.getXSize(), DstImage.getXSize());
    tSize YCount = vpl::math::getMin(SrcImage.getYSize(), DstImage.getYSize());

    //local image storage
    START_TIMER;
    float* src = NULL;
    if ((src = (float*)malloc(XCount * YCount * sizeof(float))) == NULL)
    {
        printf("error");
        
    }
    STOP_TIMER("malloc: local image storage");
    
    // Copy image to GPU
    START_TIMER;
    for( tSize y = 0; y < YCount; ++y )
    {
        for( tSize x = 0; x < XCount; ++x )
        {
            /*
             * VPL/include/VPL/System/SystemTypes.h
             * 16b unsigned values (tUInt16)
             * SrcImage(x, y);                intensity value at x,y
             * DstImage.set(x, y, newValue);  set intensity at x,y
             */
            //copy to local storage
            src[y * XCount + x] = (float)SrcImage(x, y); //(vpl::sys::tUInt16)
        }
    }
    STOP_TIMER("copy image to local storage");
    
    //free(src);
    //exit (-1);
    // O.K.
    //return true;
    
    START_TIMER;
    cl::Context context({gpu_dev.selected_Device});

    cl::Program::Sources sources;

    // kernel calculates for each element C=A+B
    std::string kernel_code=" \
    \
    __kernel void gpu_sobelX(__global const float* restrict inputImage, \
                    __global float* restrict outputImage, \
                    const int width) \
    { \
        int gid = get_global_id(0); \
        \
            outputImage[gid]    = (2 * inputImage[gid + 1]  \
                                + inputImage[gid - width + 1] \
                                + inputImage[gid + width + 1 ] \
                                - 2 * inputImage[gid - 1] \
                                - inputImage[gid - width - 1] \
                                - inputImage[gid + width - 1] \
                                )/4 ; \
         \
    }";
    sources.push_back({kernel_code.c_str(), kernel_code.length()});
        
    cl::Program program(context,sources);
    if(program.build({gpu_dev.selected_Device})!=CL_SUCCESS){
        std::cout<<" Error building: "<<program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(gpu_dev.selected_Device)<<"\n";
        exit(1);
    }
    STOP_TIMER("ocl init");
    
    START_TIMER;
    cl::Buffer buffer_src(context,CL_MEM_READ_ONLY,(XCount * YCount * sizeof(float)));
    cl::Buffer buffer_dst(context,CL_MEM_WRITE_ONLY,(XCount * YCount * sizeof(float)));
    cl_int width = XCount;
    STOP_TIMER("ocl create buffers");
    
    //create queue to which we will push commands for the device.
    cl::CommandQueue queue(context,gpu_dev.selected_Device);
    
    START_TIMER;
    queue.enqueueWriteBuffer(buffer_src,CL_TRUE,0,(XCount * YCount * sizeof(float)),src);
    STOP_TIMER("ocl enqueue write buffer");
    
    START_TIMER;
    cl::Kernel gpu_sobelX(program, "gpu_sobelX");
    gpu_sobelX.setArg(0, buffer_src);
    gpu_sobelX.setArg(1, buffer_dst);
    gpu_sobelX.setArg(2, width);
    STOP_TIMER("ocl kernel+arguments");
    START_TIMER;
    queue.enqueueNDRangeKernel(gpu_sobelX,cl::NDRange((int)XCount),cl::NDRange((int)(XCount * YCount - XCount))/*, cl::NullRange ,NULL,NULL*/);
    STOP_TIMER("ocl enqueue kernel");
    START_TIMER;
    queue.finish();
    STOP_TIMER("ocl queue finish");
    
    START_TIMER;
    float* dst = NULL;
    if ((dst = (float*)malloc(XCount * YCount * sizeof(float))) == NULL)
    {
        printf("error");
        
    }
    STOP_TIMER("malloc: dst buffer");
    START_TIMER;
    queue.enqueueReadBuffer(buffer_dst,CL_TRUE,0,(XCount * YCount * sizeof(float)),dst);
    STOP_TIMER("ocl enqueue read buffer"); 
    
    START_TIMER;
    queue.finish();
    STOP_TIMER("ocl queue finish");
    
    START_TIMER;
    #pragma omp parallel for schedule(static) default(shared)
    for( tSize y = 0; y < YCount; ++y )
    {
        for( tSize x = 0; x < XCount; ++x )
        {
            /*
             * VPL/include/VPL/System/SystemTypes.h
             * 16b unsigned values (tUInt16)
             * SrcImage(x, y);                intensity value at x,y
             * DstImage.set(x, y, newValue);  set intensity at x,y
             */
            //copy to DstImage   base::normalize(dst[y * XCount + x])
            //std::cout<< "Povodny: "<< dst[y * XCount + x] << " Novy: " << dst[y * XCount + x]/2 <<" N: "<<  base::normalize((vpl::sys::tUInt16)(dst[y * XCount + x]/2)) << "\n";
            if (((y * XCount + x) < 2*XCount) || ((y * XCount + x) > YCount * XCount - 2*XCount))
                DstImage.set(x, y, base::normalize(0));
            else
                DstImage.set(x, y, base::normalize((dst[y * XCount + x])));
        }
    }
    STOP_TIMER("normalize image and copy to DstImage");
    
    //std::cout<< (double)(end-start) <<"\n";
    //exit(0);
    
    free(dst);
    free(src);
    
    return true;
}

} //namespace img
    
} //namespace vpl