#==============================================================================
# This file comes from MDSTk software and was modified for
#
# VPL - Voxel Processing Library
# Changes are Copyright 2014 3Dim Laboratory s.r.o.
# All rights reserved.
#
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
#
# The original MDSTk legal notice can be found below.
# 
# Medical Data Segmentation Toolkit (MDSTk)
# Copyright (c) 2007 by PGMed@FIT
#
# Authors: Miroslav Svub, svub@fit.vutbr.cz
#          Michal Spanel, spanel@fit.vutbr.cz
# Date:    2007/08/25
#==============================================================================

IF ( BUILD_TESTS )

  # System libraries necessary for building binaries
  if ( WIN32 )
    set( VPL_SYSTEM_LIBS winmm kernel32 )
  else()
    if( APPLE )
      set( VPL_SYSTEM_LIBS pthread )
    else()
      if( UNIX )
        set( VPL_SYSTEM_LIBS pthread rt )
      else( UNIX )
        message( FATAL_ERROR "Unknown target system!" )
      endif()
    endif()
  endif()

  # VPL libraries
  set( VPL_LIBS ${VPL_IMAGEIO_LIB} ${VPL_IMAGE_LIB} ${VPL_MODULE_LIB} ${VPL_SYSTEM_LIB} ${VPL_MATH_LIB} ${VPL_BASE_LIB} )
  
  # TinyXML
  if( MDS_XML_ENABLED )
    list( APPEND VPL_LIBS ${VPL_TINYXML_LIB} )
  endif( MDS_XML_ENABLED )

  # OpenCV
  if( MDS_OPENCV_ENABLED )
    list( APPEND VPL_LIBS ${VPL_OPENCV} )
  endif( MDS_OPENCV_ENABLED )

  # build all tests
  ADD_SUBDIRECTORY( BaseTEST )
  ADD_SUBDIRECTORY( ImageTEST )
  ADD_SUBDIRECTORY( LogTEST )
  ADD_SUBDIRECTORY( MathTEST )
  ADD_SUBDIRECTORY( ModuleTEST ) 
  ADD_SUBDIRECTORY( SystemTEST )
  
ENDIF( BUILD_TESTS )
