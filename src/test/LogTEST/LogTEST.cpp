//==============================================================================
/* This file is part of
 * 
 * VPL - Voxel Processing Library
 * Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

#ifdef _OPENMP
#include <omp.h>
#endif

#include <iostream>
#define VPL_LOG_LEVEL LL_DEBUG
#include <VPL/Base/Logging.h>

// STL
#include <string>
#include <iostream>


//==============================================================================
/*!
 * main
 */
int main(int argc, const char *argv[])
{
	VPL_LOG_ADD_CERR_APPENDER;
//     vpl::base::CLogLayoutFormatted *layout(new vpl::base::CLogLayoutFormatted());
//     layout->setDefaultStrings();
// 	VPL_LOG_ADD_IDE_APPENDER->setLayout(layout);
 	VPL_LOG_ADD_IDE_APPENDER
   
// Simple log
	VPL_LOG_TRACE("Trace");
	VPL_LOG_DEBUG("Debug");
	VPL_LOG_INFO("Info");
	VPL_LOG_WARN_TAG("Warning", 1);
	VPL_LOG_WARN("Warning " << 2);
    VPL_LOG_WARN_RAW("Warning with raw message");
	VPL_LOG_ERROR("Error");
	VPL_LOG_FATAL("Fatal");

// Conditional log
    VPL_LOG_TRACE_IF(true, "Log trace if true");
    VPL_LOG_TRACE_IF(false, "Log trace if false");
    VPL_LOG_TRACE_IF_NOT(true, "Log trace if not true");
    VPL_LOG_TRACE_IF_NOT(false, "Log trace if not false");

    VPL_LOG_DEBUG_IF(true, "Log trace if true");
    VPL_LOG_DEBUG_IF(false, "Log trace if false");
    VPL_LOG_DEBUG_IF_NOT(true, "Log trace if not true");
    VPL_LOG_DEBUG_IF_NOT(false, "Log trace if not false");

    VPL_LOG_INFO_IF(true, "Log trace if true");
    VPL_LOG_INFO_IF(false, "Log trace if false");
    VPL_LOG_INFO_IF_NOT(true, "Log trace if not true");
    VPL_LOG_INFO_IF_NOT(false, "Log trace if not false");

    VPL_LOG_WARN_IF(true, "Log trace if true");
    VPL_LOG_WARN_IF(false, "Log trace if false");
    VPL_LOG_WARN_IF_NOT(true, "Log trace if not true");
    VPL_LOG_WARN_IF_NOT(false, "Log trace if not false");

    VPL_LOG_ERROR_IF(true, "Log trace if true");
    VPL_LOG_ERROR_IF(false, "Log trace if false");
    VPL_LOG_ERROR_IF_NOT(true, "Log trace if not true");
    VPL_LOG_ERROR_IF_NOT(false, "Log trace if not false");

    VPL_LOG_FATAL_IF(true, "Log trace if true");
    VPL_LOG_FATAL_IF(false, "Log trace if false");
    VPL_LOG_FATAL_IF_NOT(true, "Log trace if not true");
    VPL_LOG_FATAL_IF_NOT(false, "Log trace if not false");

#ifdef _OPENMP
	#pragma omp parallel num_threads(4)
	{
		int i = omp_get_thread_num();
		for(int j = 0; j < 30; ++j)
			VPL_LOG_INFO("Logging from thread: " << i);
	}
#endif
    return 0;
}

