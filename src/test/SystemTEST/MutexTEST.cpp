//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2005 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2003/11/01                       
 *
 * Description:
 * - Testing of the vpl::CMutex class.
 */

#include <VPL/System/Mutex.h>
#include <VPL/System/Sleep.h>
#include <VPL/System/Thread.h>

#include <cstdlib>

// STL
#include <iostream>


//==============================================================================
/*
 * Global constants.
 */

//! The number of threads
#define THREADS         5

//! Delay after releasing mutex
#define DELAY           50

//! Smart pointer to mutex
vpl::sys::CMutexPtr     spMutex;


//==============================================================================
/*!
 * Generates random number
 */
unsigned random(const unsigned uiMax)
{
    return (1 + (unsigned int)(((double)rand() / RAND_MAX) * uiMax));
}


//==============================================================================
/*!
 * Thread routine
 */
VPL_THREAD_ROUTINE(thread)
{
    int id = *(reinterpret_cast<int *>(pThread->getData()));

    VPL_THREAD_MAIN_LOOP
    {
        spMutex->lock();
        std::cout << "Thread: ";
        vpl::sys::sleep(random(DELAY));
        std::cout << id;
        vpl::sys::sleep(random(DELAY));
        std::cout << std::endl;
        spMutex->unlock();
        vpl::sys::sleep(random(DELAY));
    }

    spMutex->lock();
    std::cout << "Thread: " << id << " terminated" << std::endl;
    spMutex->unlock();

    return 0;
}


//==============================================================================
/*!
 * main
 */
int main(int argc, const char *argv[])
{
    // Threads
    vpl::sys::CThread *ppThreads[THREADS];
    int piThreadsId[THREADS];

    // Creation of all ppThreads
    for( int i = 0; i < THREADS; i++ )
    {
        piThreadsId[i] = i;
        ppThreads[i] = new vpl::sys::CThread(thread, (void *)&piThreadsId[i], true);
    }

    // Sleep
    vpl::sys::sleep(10000);

    // Destroy all ppThreads
    for( int i = 0; i < THREADS; i++ )
    {
        ppThreads[i]->terminate(true, 1000);
        delete ppThreads[i];
    }

    return 0;
}

