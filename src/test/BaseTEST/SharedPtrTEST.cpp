//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2005 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2004/11/22                       
 *
 * Description:
 * - Testing of the vpl::CRefCountPtr template.
 */

#include <VPL/Base/SharedPtr.h>

// STL
#include <iostream>


//==============================================================================
/*!
 * Class CA
 */
class CA : public vpl::base::CObject
{
public:
    //! Smart pointer type
    //! - declares type tPointer
    VPL_SHAREDPTR(CA);

public:
    //! Internal data
    int m_iData;

public:
    //! Constructor
    CA() : m_iData(0)
    {
        std::cout << "  <" << this << "> CA::CA()" << std::endl;
    }

    //! Constructor
    CA(int iData) : m_iData(iData)
    {
        std::cout  << "  <" << this << "> CA::CA(" << iData << ")" << std::endl;
    }

    //! Copy constructor
    CA(const CA& a) : vpl::base::CObject(), m_iData(a.m_iData)
    {
        std::cout  << "  <" << this << "> CA::CA(" << &a << ")" << std::endl;
    }

    //! Destructor
    virtual ~CA()
    {
        std::cout << "  <" << this << "> CA::~CA()" << std::endl;
    }

    //! Print message
    virtual void print()
    {
        std::cout << "  <" << this << "> CA::print()" << std::endl;
        std::cout << "    m_iData = " << m_iData  << std::endl;
        std::cout << "    m_iReferences = " << getReferencesCount() << std::endl;
        std::cout << "    isOnHeap() returns " << isOnHeap() << std::endl;
    }
};

//! Smart pointer to the class CA
typedef CA::tSmartPtr CAPtr;


//==============================================================================
/*!
 * Class CAA
 */
class CAA : public CA
{
public:
    //! Smart pointer type
    //! - declares type tPointer
    VPL_SHAREDPTR(CAA);

public:
    //! Additional internal data
    int m_iData2;

public:
    //! Constructor
    CAA() : m_iData2(0)
    {
        std::cout << "  <" << this << "> CAA::CAA()" << std::endl;
    }

    //! Constructor
    CAA(int iData, int iData2) : CA(iData), m_iData2(iData2)
    {
        std::cout  << "  <" << this << "> CAA::CAA(" << iData << ", " << iData2 << ")" << std::endl;
    }

    //! Copy constructor
    CAA(const CAA& a) : CA(a), m_iData2(a.m_iData2)
    {
        std::cout  << "  <" << this << "> CAA::CAA(" << &a << ")" << std::endl;
    }

    //! Destructor
    virtual ~CAA()
    {
        std::cout << "  <" << this << "> CAA::~CAA()" << std::endl;
    }

    //! Print message
    virtual void print()
    {
        std::cout << "  <" << this << "> CAA::print()" << std::endl;
        std::cout << "    m_iData = " << m_iData  << std::endl;
        std::cout << "    m_iData2 = " << m_iData2  << std::endl;
        std::cout << "    m_iReferences = " << getReferencesCount() << std::endl;
        std::cout << "    isOnHeap() returns " << isOnHeap() << std::endl;
    }
};

//! Smart pointer to the class CAA
typedef CAA::tSmartPtr CAAPtr;


//==============================================================================
/*!
 * Waiting for a key
 */
void keypress()
{
    while( std::cin.get() != '\n' );
}


//==============================================================================
/*!
 * main
 */
int main(int argc, const char *argv[])
{
    std::cout << "Create CRefCountPtr<CA> p1 using CA() default constructor" << std::endl;
    CAPtr p1;
    p1->print();
    keypress();

    std::cout << "Create p2 using CA(const int) constructor" << std::endl;
    {
//        CAPtr p2(1);
        CAPtr p2(new CA(1));
        p2->print();
        keypress();
    }

    std::cout << "Print p1" << std::endl;
    p1->print();
    keypress();

    std::cout << "Create p2 as new copy of the p1 using CA::CA(const CA&)" << std::endl;
    {
//        CAPtr p2(*p1);
        CAPtr p2(new CA(*p1));
        p2->print();
        keypress();

        std::cout << "Increment data of the p2" << std::endl;
        p2->m_iData += 1;
        p2->print();
        keypress();
    }

    std::cout << "Print p1" << std::endl;
    p1->print();
    keypress();

    std::cout << "Create p2 as reference to the p1 using CRefCountPtr(const CRefCountPtr&)" << std::endl;
    {
        CAPtr p2(p1);
        p2->print();
        keypress();

        std::cout << "Increment data of the p2" << std::endl;
        p2->m_iData += 1;
        p2->print();
        keypress();
    }

    std::cout << "Print p1" << std::endl;
    p1->print();
    keypress();


    std::cout << "Create CRefCountPtr<CAA> pp1 using CAA() default constructor" << std::endl;
    CAAPtr pp1;
    pp1->print();
    keypress();

    std::cout << "Create pp2 using CAA(const int, const int) constructor" << std::endl;
    {
//        CAAPtr pp2(1, 2);
        CAAPtr pp2(new CAA(1, 2));
        pp2->print();
        keypress();
    }

    std::cout << "Print pp1" << std::endl;
    pp1->print();
    keypress();

    std::cout << "Create pp2 as new copy of the pp1 using CAA::CAA(const CAA&)" << std::endl;
    {
//        CAAPtr pp2(*pp1);
        CAAPtr pp2(new CAA(*pp1));
        pp2->print();
        keypress();

        std::cout << "Increment data of the pp2" << std::endl;
        pp2->m_iData2 += 1;
        pp2->m_iData += 1;
        pp2->print();
        keypress();
    }

    std::cout << "Print pp1" << std::endl;
    pp1->print();
    keypress();

    std::cout << "Create pp2 as reference to the pp1 using CRefCountPtr(const CRefCountPtr&)" << std::endl;
    {
        CAAPtr pp2(pp1);
        pp2->print();
        keypress();

        std::cout << "Increment data of the pp2" << std::endl;
        pp2->m_iData += 1;
        pp2->m_iData2 += 1;
        pp2->print();
        keypress();
    }

    std::cout << "Print pp1" << std::endl;
    pp1->print();
    keypress();


    std::cout << "Create p2 as pointer to the pp1 using CRefCountPtr(T *)" << std::endl;
    {
        CAPtr p2((CA *)pp1);
        p2->print();
        keypress();

        std::cout << "Increment data of the p2" << std::endl;
        p2->m_iData += 1;
        p2->print();
        keypress();
    }

    std::cout << "Print pp1" << std::endl;
    pp1->print();
    keypress();

    std::cout << "Create p2 using CA(const int) constructor" << std::endl;
    {
//        CAPtr p2(1);
        CAPtr p2(new CA(1));
        p2->print();
        keypress();

        std::cout << "Assign pp1 to the p2" << std::endl;
        p2 = (CA *)pp1;
        p2->print();
        keypress();
    }

    std::cout << "Print pp1" << std::endl;
    pp1->print();
    keypress();

    std::cout << "Create CA object a1 using CA() default constructor" << std::endl;
    CA a1;
    a1.print();
    keypress();

    std::cout << "Create p2 as pointer to the a1 using CRefCountPtr(T *)" << std::endl;
    {
        CAPtr p2(&a1);
        p2->print();
        keypress();

        std::cout << "Increment data of the p2" << std::endl;
        p2->m_iData += 1;
        p2->print();
        keypress();
    }

    std::cout << "Print a1" << std::endl;
    a1.print();
    keypress();

    std::cout << "Exit" << std::endl;

    return 0;
}

