//==============================================================================
/* This file is part of
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

#include <VPL/Base/Setup.h>

// STL
#include <iostream>
#include <ctime>
#include <cstdlib>


namespace vpl
{
namespace test
{

//==============================================================================
/*
 * Functions to measure time
 */

#ifdef _OPENMP
    //! Clock counter type
    typedef double tClockCounter;
#else
    //! Clock counter type
    typedef clock_t tClockCounter;
#endif

//! Starts time measuring
inline tClockCounter start()
{
#ifdef _OPENMP
    return omp_get_wtime();
#else
    return clock();
#endif
}

//! Stops time measuring, returns the difference from Start and prints result
inline tClockCounter stop(tClockCounter Start, bool bPrint = true)
{
#ifdef _OPENMP
    tClockCounter Count = omp_get_wtime() - Start;
#else
    tClockCounter Count = clock() - Start;
#endif
    if( bPrint )
    {
        std::cout << "  Estimated time: " << Count << std::endl;
    }
    return Count;
}


//==============================================================================
/*
 * Misc. functions
 */

//! Function waits for the Enter key...
inline void keypress()
{
    while( std::cin.get() != '\n' );
}

//! Generates random number
unsigned random(const unsigned uiMax)
{
    return (1 + (unsigned int)((double(rand()) / RAND_MAX) * uiMax));
}


} // namespace test
} // namespace vpl
