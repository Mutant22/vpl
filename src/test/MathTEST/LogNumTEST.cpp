//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2006 by Michal Spanel     \n
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.c
 * Date:    2006/09/04                          \n
 *
 * Description:
 * - Testing of the vpl::math::CLogNum template.
 */

#include <VPL/Math/LogNum.h>

// STL
#include <iostream>


//==============================================================================
/*!
 * Waiting for a key
 */
void keypress()
{
    while( std::cin.get() != '\n' );
}


//==============================================================================
/*!
 * main
 */
int main(int argc, const char *argv[])
{
    vpl::math::CDLogNum n1(vpl::math::E);
    std::cout << "Number n1(e): n1 = " << n1 << ", n1.get() = " << n1.get() << std::endl;
    std::cout << "Required result: 1, 2.71828" << std::endl;
    keypress();

    vpl::math::CDLogNum n2(0);
    std::cout << "Number n2(0): n2 = " << n2 << ", " << n2.get() << std::endl;
    std::cout << "Required result: x, 0" << std::endl;
    keypress();

    vpl::math::CDLogNum n3(0, vpl::math::LogNum::LOG_VALUE);
    std::cout << "Number n2(0, LOG_VALUE): n3 = " << n3 << ", " << n3.get() << std::endl;
    std::cout << "Required result: 0, 1" << std::endl;
    keypress();


    n3 = n1 * n2;
    std::cout << "n3 = n1 * n2:" << std::endl;
    std::cout << "n3 = " << n3 << ", n3.get() = " << n3.get() << std::endl;
    keypress();

    n3 = n1 / n2;
    std::cout << "n3 = n1 / n2:" << std::endl;
    std::cout << "n3 = " << n3 << ", n3.get() = " << n3.get() << std::endl;
    keypress();

    n3 = n1 + n2;
    std::cout << "n3 = n1 + n2:" << std::endl;
    std::cout << "n3 = " << n3 << ", n3.get() = " << n3.get() << std::endl;
    keypress();

    n3 = n1 - n2;
    std::cout << "n3 = n1 - n2:" << std::endl;
    std::cout << "n3 = " << n3 << ", n3.get() = " << n3.get() << std::endl;
    keypress();


    n1 = 5;
    std::cout << "n1 = 5:" << std::endl;
    std::cout << "n1 = " << n1 << ", n1.get() = " << n1.get() << std::endl;
    keypress();

    n3 = n1 * 2.0;
    std::cout << "n3 = n1 * 2:" << std::endl;
    std::cout << "n3 = " << n3 << ", n3.get() = " << n3.get() << std::endl;
    keypress();

    n3 = n1 / 2.0;
    std::cout << "n3 = n1 / 2:" << std::endl;
    std::cout << "n3 = " << n3 << ", n3.get() = " << n3.get() << std::endl;
    keypress();

    n3 = 2.0 + n1;
    std::cout << "n3 = 2 + n1:" << std::endl;
    std::cout << "n3 = " << n3 << ", n3.get() = " << n3.get() << std::endl;
    keypress();

    n3 = 10.0 - n1;
    std::cout << "n3 = 10 - n1:" << std::endl;
    std::cout << "n3 = " << n3 << ", n3.get() = " << n3.get() << std::endl;
    keypress();


    n1 = 0;
    std::cout << "n1 = 0:" << std::endl;
    std::cout << "n1 = " << n1 << ", n1.get() = " << n1.get() << std::endl;
    keypress();

    for( int i = 0; i < 100; ++i )
    {
        n1 += 0.5;
    }
    std::cout << "100x n1 += 0.5:" << std::endl;
    std::cout << "n1 = " << n1 << ", n1.get() = " << n1.get() << std::endl;
    keypress();

    return 0;
}

