//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2005 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2003/12/04                       
 * 
 * Description:
 * - Testing of the vpl::CVector template.
 */

#include <VPL/Math/Vector.h>
#include <VPL/Math/VectorFunctions.h>

// STL
#include <iostream>


//==============================================================================
/*!
 * Prints a given vector
 */
void printVector(vpl::math::CDVector& v)
{
    std::cout.setf(std::ios_base::fixed);
    std::cout << "  ";
    for( vpl::tSize i = 0; i < v.getSize(); i++ )
    {
        std::cout << v(i) << " ";
    }
    std::cout << std::endl;
}


//==============================================================================
/*!
 * Waiting for a key
 */
void keypress()
{
    while( std::cin.get() != '\n' );
}


//==============================================================================
/*!
 * main
 */
int main(int argc, const char *argv[])
{
    vpl::math::CDVector v1(4);
    v1.fill(1);
    std::cout << "Vector 1" << std::endl;
    printVector(v1);
    keypress();

    vpl::math::CDVector v2(4);
    std::cout << "Vector 2" << std::endl;
    for( vpl::tSize j = 0; j < v2.getSize(); j++ )
    {
        v2(j) = j;
    }
    printVector(v2);
    keypress();

    std::cout << "Operation v1 += v2" << std::endl;
    v1 += v2;
    printVector(v1);
    keypress();

    vpl::math::CDVectorPtr spV3(new vpl::math::CDVector(8));
    spV3->fill(0);
    std::cout << "New smart pointer to vector 3" << std::endl;
    printVector(*spV3);
    keypress();

    std::cout << "Fill segment of the vector spV3->asEigen().segment(0,4).fill(2)" << std::endl;
    spV3->asEigen().segment(0,4).fill(2);
    printVector(*spV3);
    keypress();

    std::cout << "Fill segment of the vector spV3->asEigen().segment(4,4).fill(5)" << std::endl;
    spV3->asEigen().segment(4,4).fill(5);
    printVector(*spV3);
    keypress();

    std::cout << "Iterate over all elements in the vector spV3" << std::endl;
    for( vpl::math::CDVector::tIterator It(*spV3); It; ++It )
    {
        std::cout << " " << *It;
    }
    std::cout << std::endl;
    keypress();

/*    vpl::math::CDVector v31(*spV3, 0, 4, vpl::REFERENCE);
    std::cout << "Reference v31" << std::endl;
    printVector(v31);
    keypress();

    vpl::math::CDVector v32(*spV3, 4, 4, vpl::REFERENCE);
    std::cout << "Reference v32" << std::endl;
    printVector(v32);
    keypress();

    std::cout << "Operation v31 = v2, v32 = v1" << std::endl;
    v31 += v2; v32 += v1;
    printVector(*spV3);
    keypress();

    std::cout << "Operation v31 *= 2, v32 += 10" << std::endl;
    v31 *= 2; v32 += 10;
    printVector(*spV3);
    keypress();*/

    std::cout << "Operation getMin(), getMax(), getSum(), getMean()" << std::endl;
    std::cout << "  " << vpl::math::getMin<double>(*spV3)
    << "  " << vpl::math::getMax<double>(*spV3)
    << "  " << vpl::math::getSum<double>(*spV3)
    << "  " << vpl::math::getMean<double>(*spV3)
    << std::endl;
    keypress();

    std::cout << "Operation v1.subSample(*spV3, 2)" << std::endl;
    v1.subSample(*spV3, 2);
    printVector(v1);
    keypress();

    std::cout << "Vector 2" << std::endl;
    printVector(v2);
    keypress();

    std::cout << "Operation spV3->concat(v1, v2)" << std::endl;
    spV3->concat(v1, v2);
    printVector(*spV3);
    keypress();

    std::cout << "Operation spV3->limit(2, 8)" << std::endl;
    spV3->limit(2, 8);
    printVector(*spV3);
    keypress();

/*    std::cout << "Operation v31.resize(16).fill(5)" << std::endl;
    v31.resize(16).fill(5);
    printVector(v31);
    keypress();

    std::cout << "Vector 3" << std::endl;
    printVector(*spV3);
    keypress();

    std::cout << "Vector 31" << std::endl;
    printVector(v31);
    keypress();

    std::cout << "Vector 32" << std::endl;
    printVector(v32);
    keypress();*/

    return 0;
}

